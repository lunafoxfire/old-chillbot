﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace Chillbot.Commands.ChillCommands
{
    class Greet : ICommand
    {
        public string CommandName { get; } = "greet";
        public string[] Aliases { get; } = { "hi", "hello" };
        public string Description { get; } = "Chillbot greets the named person.";
        public string Parameters { get; } = "<User>";

        //the bot this command is registered to
        private Bot Bot;

        private string[] greetings =
            {
                "Hello, {0}.",
                "Hello {0}!",
                "Hi {0}!",
                "Hi there, {0}!",
                "Hiya {0}!",
                "Hey {0}!",
                "Hey there {0}!",
                "Ohai {0}",
                "Ohai thar {0}",
                "Heyo {0}",
                "Hullo, {0}!",
                "Hullo thar {0}!",
                "How's it going, {0}?",
                "How goes it, {0}?",
                "Hail {0}!!",
                "Greetings, {0}."
            };

        //registers the command to the given bot
        public void RegisterTo(Bot bot)
        {
            Bot = bot;

            Bot.Commands.CreateCommand(CommandName)
                .Alias(Aliases)
                .Description(Description)
                .Parameter("_greetedPerson", ParameterType.Required)
                .Do(e => { Execute(e); });
        }

        public async void Execute(CommandEventArgs e)
        {
            string greetedPerson = e.GetArg("_greetedPerson");

            //check if the parameters are an actual user
            if (DiscordHelperClass.IsStringMentionID(greetedPerson))
            {
                //check if the user is the bot itself
                if (greetedPerson != $"<@{Bot.ID}>")
                {
                    if (Bot.EnableCommandDeletion == true)
                    {
                        await e.Message.Delete();
                    }
                    //send a random greeting
                    int greetingIndex = Bot.Rng.Next(0, greetings.Length);
                    string message = string.Format(greetings[greetingIndex], greetedPerson);
                    await e.Channel.SendMessage(message);
                }
                //if bot is the subject
                else
                {
                    await e.Channel.SendMessage(":smile_cat:");
                }
            }
        }
    }
}
