﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Chillbot.Commands;

namespace Chillbot
{
    class Bot
    {
        //set to false before compile/release
        bool isDevBot = true;

        //discord hooks
        public DiscordClient _client { get; private set; }
        public CommandService Commands { get; private set; }

        //bot credentials
        public string ID { get; private set; } = "245722865406574592";
        string token = "MjQ1NzIyODY1NDA2NTc0NTky.CwQTcQ.FCsrfBtHGHp0wTxVNAg2a2c4tGI";

        //Random number generator
        public Random Rng = new Random();

        //Enables commands to delete the message that invoked them
        public bool EnableCommandDeletion = true;

        public Bot()
        {
            if (isDevBot == true)
            {
                ID = "246040396247859213";
                token = "MjQ2MDQwMzk2MjQ3ODU5MjEz.CwU2lQ.PZQaqj4M-gCDSA7Z-54OX821_x4";
            }
            else
            {
                ID = "245722865406574592";
                token = "MjQ1NzIyODY1NDA2NTc0NTky.CwQTcQ.FCsrfBtHGHp0wTxVNAg2a2c4tGI";
            }

            //initialize client
            _client = new DiscordClient(x =>
            {
                x.LogLevel = LogSeverity.Info;
                x.LogHandler = Log;
            });

            //initialize commands service
            _client.UsingCommands(x =>
            {
                x.HelpMode = HelpMode.Private;
                x.PrefixChar = '~';
                x.AllowMentionPrefix = true;
            });

            Commands = _client.GetService<CommandService>();


            //register commands here
            RegisterCommand<Commands.ChillCommands.Greet>();
            RegisterCommand<Commands.ChillCommands.Tell>();
            RegisterCommand<Commands.DiceCommands.Roll>();


            //connect to server
            _client.ExecuteAndWait(async () =>
            {
                await _client.Connect(token, TokenType.Bot);
            });
        }

        //registers the given command to the bot
        private void RegisterCommand<T>() where T:ICommand, new()
        {
            T command = new T();
            command.RegisterTo(this);
        }


        //debug log
        private void Log(object sender, LogMessageEventArgs e)
        {
            Console.WriteLine(e.Message);
        }
    }
}
